import React from 'react';
import { createStackNavigator } from 'react-navigation';
import FirstPage from '../screens/FirstPage';

import MainTabNavigator from './MainTabNavigator';

export default createStackNavigator({
  Main: {
    screen:MainTabNavigator,
    navigationOptions: {
      title:'ciao'
    }

  },
  FirstPage: {
    screen: FirstPage,
  },
});
