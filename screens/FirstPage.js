import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import {MapView,Location, Permissions} from "expo";

export default class FirstPage extends React.Component {
  constructor(props){
    super(props);
    this.state= {
      lat:null,
      lng:null
    }
  }
async componentDidMount() {
  let {status} = await Permissions.askAsync(Permissions.LOCATION);
            if (status !== 'granted') {
                Alert('No active geolocalization')
            }
            else {
                let location = await Location.getCurrentPositionAsync();
                this.setState({
                      lat: location.coords.latitude,
                      lng: location.coords.longitude,
                  })
                }
            }


  render() {
    return (
      <ScrollView style={styles.container}>
      {this.state.lat && this.state.lng &&

        <MapView
                           style={{height: 160}}
                           initialRegion={{
                               latitude: this.state.lat,
                               longitude: this.state.lng,
                               latitudeDelta: 0.0022,
                               longitudeDelta: 0.0021,
                           }}
                           showsUserLocation={true}
                       />
      }

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
